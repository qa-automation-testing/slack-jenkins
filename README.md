# Slack Jenkins
## Integration Slack-Jenkins by Slack App

La integracion de Jenkins con Slack se va a realizar con la nueva funcionalidad de Slack, **Slack App**.
El objetivo es informar a un canal especifico de slack el status del job ejecutado en Jenkins. Este puede ser un mensaje predefinido o customizado.


## 1 - Envio de notificaciones utilizando la app de Jenkins CI en Slack

Al igual que Jenkins, Slack cuenta con "plugins" predefinidos que facilitan la integracion con distintas herramientas. La desventaja es que en la version free solo se pueden instalar hasta 10 apps.

Configuracion Slack:
* Instalar Jenkins CI app: Desde tu workspace de slack ir a *Setting & administration -> Manage apps*.  Allí buscar el app de Jenkins, click en "Add to Slack", seleccionar el canal de posteo y agregar el app. Una vez hecho esto, vas a ver la configuracion y un ejemplo de como usarlo, lo importante es copiar el **Team subdomain** y **token**.

Configuracion Jenkins:
* Instalar plugin: Desde el gestor de plugins, buscar **Slack Notification** e instalarla.
* Agregar token: ir al administrador de credenciales y agregar un token de tipo **Secret text** donde se va a pegar el token del paso anterior.
* Configurar job: Si es necesario crear uno nuevo de tipo Free Style. En la configuracion, en "Post Actions" agregar *Slack Notifications* y en *Advanced* pegar en **Workspace** el *Team subdomain*, seleccionar la credencial creada y agregar el **Channel** al cual se va a postiar. Para probar la integracion, click en "Test Connection". Para terminar check en los **Notify** que se quieran, por ejemplo *Notify Succes*

Ejecucion:
* Una vez probado la integracion, solo es necesario hacer un *Build* del proyecto y ver como llegan a Slack las notificaciones. Los *Notify* de la configuracion del plugin son mensajes predefinidos, si se quiere agregar información adicional hay que habilitar **Custom Message**


## 2 - Envio de notificaciones utilizando Slack App

En este caso, se va a crear una **aplicacion** propia de Slack. La ventaja es que dicha app es bastante customizable agregando los **features** que se requieran. Para nuestro ejemplo solo es necesario el feature de *autenticacion y permisos*

Configuracion Slack:
* Crear App: Nuevamente vamos a *Setting & administration -> Manage apps* y desde esa pagina presionamos **Build**, luego **Create New App** y asignamos un nombre y el workspace donde se va a integrar.
* Configurar App: Como se menciono anteriormente hay bastantes cosas que se puedan hacer con nuestra app. Vamos **OAuth & Permissions**, en la sección de **Bot Token Scopes** agregamos un OAuth del tipo **chat:write**. Agregado este feature, se puede instalar nuestra app presionando **Install App to Workspace** y vemos que se genera un token el cual copiamos.
* Agregar app a un canal: En el paso anterior basicamente se puede decir que se creo un *bot* que puede postear mensajes a un channel especifico. Para terminar con la configuración, volvemos al workspace e invitamos al canal deseado la nueva app mediante @app_name.

Configuracion Jenkins:
* Agregar token: Vamos al administrador de credenciales y agregamos uno nuevo del tipo *Secret text* donde pegamos el token generado anteriormente.
* Configurar job: Usando el job anterior, haciendo una copia del mismo o creando uno nuevo, nos dirigimos a las opciones avanzadas del plugin de Slack y allí los importante es agregar la credencial correspondiente, el canal al cual el *bot* va a publicar y algo muy importante (que no se menciono en el ejemplo anterior) es tildar la opción **Custom slack app bot user**, sin esto último la integración no funciona. Para probar presionamos *Test Connection* y sí es *Succces* esta todo ok.

Ejecución:
* Una vez guardada la configuración, hacemos el *Build* del job y observamos que lleguen al canal correspondiente el status del job. Si se quisiera publicar en mas de un channel simplemente lo agregamos a la configuración del job, pero tener en cuenta que tambien hay que invitar al *bot* a dichos canales.
